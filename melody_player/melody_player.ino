/*
 * Language: Arduino Language
 * Author:   Vekshin Roman
 * Date:     15.04.2017
 * Name:     Melody player
 */

/** Input pins */
/* Buttons */
#define B_NEXT_SONG 9
#define B_PLAY_SONG 8

/** Output pins */
/* Beeper(buzzer) or simple speaker */
#define SPEAKER 2
/* Pin connected to SH_CP of 74HC595 */
#define CLOCK_PIN 11
/* Pin connected to ST_CP of 74HC595 */
#define LATCH_PIN 12.
/* Pin connected to DS    of 74HC595 */
#define DATA_PIN 13

// void noti(String const notes, int const time);

/* Initialization */
void setup() {
  pinMode(B_NEXT_SONG, INPUT_PULLUP);
  pinMode(B_PLAY_SONG, INPUT_PULLUP);

  pinMode(SPEAKER, OUTPUT);
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);

  Serial.begin(9600);
  Serial.println("Arduino was successfully launched");
  Serial.println("Start melody");
  noti("D#71F#71B 71", 70);
}

/* Convenient buzzer use */
void speak(int const hz, int const time) {
  tone(SPEAKER, hz, time);
  /* To separate sounds */
  delay(time);
}

/* Error informing */
void error(int const number) {
  String const ERRORS_STRING_ARRAY[] = {
      "Error reading music", "Overrange the array of music",
      "Overrange the array of errors", /* :DDDDDDD */
      "Overrange the array of segments"};

  if (number + 1 > (sizeof(ERRORS_STRING_ARRAY) / sizeof(String))) {
    error(2);
  } else {
    Serial.println(ERRORS_STRING_ARRAY[number]);
    Serial.println("error #" + (String)(number));
  }

  for (int x = 0; x < number + 1; x++) {
    speak(1000, 75);
    speak(2000, 75);
  };
}

/* Пищим нотами */
void noti(String const notes, int const time) {
  String const ideal = "C C#D D#E F F#G G#A A#B ";
  int k, m;
  Serial.println("Plays a melody");

  for (int i = 0; i < notes.length() - 1; i += 4) {
    if (ideal.indexOf(notes.substring(i, i + 2)) > -1) {
      k = (ideal.indexOf(notes.substring(i, i + 2)) >> 1) - 9;
      m = (int(notes.charAt(i + 2))) - 52;
      m = m * 12 + k;
      speak(440 * pow(2, m / 12.0),
            time * pow(2, -(int(notes.charAt(i + 3)) - 49)));
      delay(time >> 4);
    } else {
      if (notes.substring(i, i + 1) == " ") {
        delay(time * pow(2, -(int(notes.charAt(i + 3)) - 49)));
      } else {
        error(0);
        break;
      }
    }
  }

  Serial.println("Playing music is finished");
}

/*
 * Song names
 * 0. ???
 * 1. ???
 * 2. ???
 * 3. ???
 */
String const melodyList[] = {
    "C 63   4G#64F 65G 65F 65G 65F 64G 64E 63C 63C 64E 64G 64A 63E 63A 63B "
    "63G#64G 64G#64G 64G 64",
    /* "C 73   4G#74F 75G 75F 75G 75F 74 G 74E 73C 73   4C 74E 74G 74A 73E 73A
       73B 73G#74G 74G#74G 74G 74", */
    "E 74G 74E 74G 74E 74D 74E 74C 74B 64C 74D 74E 74D 74C 74G 74E 74G 74E 74G "
    "74E 74D 74E 74C 74B 64C 74D 74F 74E 74D 74C 74",
    "E 64E 64E 64C 64E 64G 64G 54C 64G 54E 54A 54C 64A#54A 54G 54E 64G 64A 64F "
    "64G 64E 64C 64D 64C 64",
    "A 74A 74A 73G 74G 74G 73F 74F 74F 73E 73F 73G 74G 74G 73F 74F 74F 73E 73E "
    "73F 73E 73",
    "A 73G#75A 75B 74A 74F 74F 75E 75D 73F 74A 74A 74D 83A 74C 84B 74B 73",
    "E 83C 84C 84E 83C 84C 84E 84D 84C 84B 74A 72F 83A 84F 84E 83C 84C 84E 84D "
    "84C 84B 74A 72"};

void printNumber(int num) {
  byte const segments[10] = {0b01111101, 0b00100100, 0b01111010, 0b01110110,
                             0b00100111, 0b01010111, 0b01011111, 0b01100100,
                             0b01111111, 0b01110111};
  /*
   * byte const segments[10] = {
   * 0x7D, 0x24, 0x7A, 0x76, 0x27,
   * 0x57, 0x5F, 0x64, 0x7F, 0x77
   * };
   */
  digitalWrite(LATCH_PIN, LOW);
  if (num + 1 > (sizeof(segments) / sizeof(byte)))
    error(3);
  else
    shiftOut(DATA_PIN, CLOCK_PIN, LSBFIRST, segments[num]);
  digitalWrite(LATCH_PIN, HIGH);
}

String mp = "";
String mpp = "";
int clicks = 0;
boolean buttonWasUp = true;

/* Main body */
void loop() {
  /* Play music from the serial port */
  if (Serial.available() > 0) Serial.println("There is data to read");

  while (Serial.available() > 0) {
    mpp = mp;
    char temp = (Serial.read());
    mp += (temp);
    delay(1);
  };

  if ((mp.length() > 0)) {
    noti(mp, 640);
    mp = "";
  }

  if (buttonWasUp && digitalRead(B_NEXT_SONG)) {
    delay(10);

    if (digitalRead(B_NEXT_SONG))
      clicks = (clicks + 1) % ((sizeof(melodyList) / sizeof(String)));

    Serial.println("Song " + (String)clicks);
  }

  buttonWasUp = !digitalRead(B_NEXT_SONG);
  printNumber(clicks);

  if (digitalRead(B_PLAY_SONG)) {
    if (clicks + 1 > (sizeof(melodyList) / sizeof(String))) {
      error(1);
    } else {
      noti(melodyList[clicks], 800);
    }

    delay(100);
  };
}
